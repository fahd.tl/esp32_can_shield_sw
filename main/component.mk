#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_ADD_INCLUDEDIRS += . ../mcp2515 ../queue ../inc
COMPONENT_SRCDIRS += . ../mcp2515 ../queue
