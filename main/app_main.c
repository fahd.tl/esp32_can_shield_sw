/*
 * Copyright © 2020 by Fahd Tauil
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

/**
 *
 * \file app_main.c
 *
 * \date 2020-03-20 00:19:27
 *
 * \author Fahd Tauil (tauil.fahd@gmail.com)
 *
 * \brief
 *
 */

#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"
#include "soc/gpio_struct.h"
#include "soc/gpio_sd_struct.h"
#include "driver/spi_master.h"
#include <esp_timer.h>

#include "std_types.h"
#include "mcp2515.h"
#include "mcp2515_cfg.h"
#include "mcp2515_private.h"

#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG
#include "esp_log.h"

static const char* TAG = "MAIN_FUNC";

#define SPI_BUFFER_MAX_SIZE (20)

#define SPI_PIN_MISO (12)
#define SPI_PIN_MOSI (13)
#define SPI_PIN_CLK  (14)
#define SPI_PIN_CS   (15)

#define SPI_MODE (0)

#define SPI_CLOCK_SPEED_HZ (8000000)

#define STACK_SIZE  (1024)

#define DEBUG_MODE 0

spi_device_handle_t spi_handle_mst;
portMUX_TYPE mutex_gst = portMUX_INITIALIZER_UNLOCKED;

/*
 * F U N C T I O N S   P R O T O T Y P E S
 */
void spi_init_mvd ( spi_device_handle_t * spi_handle_fpst );
StdReturn_gui8_t spi_write_mui8 ( uint8_t cmd_fui8, uint8_t addr_fui8, uint8_t * const send_buffer_fpui8, uint32_t buffer_len_fu32 );
StdReturn_gui8_t spi_read_mui8 ( uint8_t cmd_fui8, uint8_t addr_fui8, uint8_t * rec_buffer_fpui8, uint32_t buffer_len_fu32 );
StdReturn_gui8_t get_time_stamp_mui8 ( TimeStamp_gst_t * time_stamp_fpst );
void main_init_mvd ( spi_device_handle_t * spi_handle_fpst );
void task_10ms_mvd ( void * dummy_fpvd );
void AtomicOpStart_gvd ( void );
void AtomicOpEnd_gvd ( void );


StdReturn_gui8_t spi_write_mui8( uint8_t cmd_fui8, uint8_t addr_fui8, uint8_t * buffer_fpui8, uint32_t buffer_len_fu32 )
{
  spi_transaction_ext_t spi_trans_lst;
  esp_err_t result_lsi32;
  StdReturn_gui8_t last_result_lui8 = OP_OK;

  memset( &spi_trans_lst , 0 , sizeof(spi_transaction_ext_t) );

  spi_trans_lst.base.flags    = SPI_TRANS_VARIABLE_ADDR | SPI_TRANS_VARIABLE_CMD;
  spi_trans_lst.base.cmd      = cmd_fui8;
  spi_trans_lst.command_bits  = 8;
  spi_trans_lst.base.length   = 8;
  if ( addr_fui8 != SPI_NO_ADDR ) {
    spi_trans_lst.address_bits = 8;
    spi_trans_lst.base.addr    = addr_fui8;
    spi_trans_lst.base.length += 8;
  }
  switch ( cmd_fui8 ) {
    // write cases
    case SPI_WRITE_INST:
    case SPI_LOAD_TXB0_ALL_INST:
    case SPI_LOAD_TXB0_DATA_INST:
    case SPI_LOAD_TXB1_ALL_INST:
    case SPI_LOAD_TXB1_DATA_INST:
    case SPI_LOAD_TXB2_ALL_INST:
    case SPI_LOAD_TXB2_DATA_INST:
    case SPI_RTS_TXB0_INTS:
    case SPI_RTS_TXB1_INTS:
    case SPI_RTS_TXB2_INTS:
    case SPI_BIT_MODIFY_INST:
    case SPI_RESET_INST: {
      spi_trans_lst.base.length += 8 * buffer_len_fu32;
      spi_trans_lst.base.tx_buffer = buffer_fpui8;
      break;
    }
      // read cases
    case SPI_READ_INST:
    case SPI_READ_RXB0_HEADER_INST:
    case SPI_READ_RXB0_PAYLOAD_INST:
    case SPI_READ_RXB1_HEADER_INST:
    case SPI_READ_RXB1_PAYLOAD_INST:
    case SPI_READ_STATUS_INST:
    case SPI_RX_STATUS_INST: {
      spi_trans_lst.base.length += 8 * buffer_len_fu32;
      spi_trans_lst.base.rxlength = 8 * buffer_len_fu32;
      spi_trans_lst.base.rx_buffer = &buffer_fpui8[ 0 ];
      break;
    }
    default:
      last_result_lui8 = OP_NOK;
  }
  if ( last_result_lui8 == OP_OK ) {
    result_lsi32 = spi_device_polling_transmit( spi_handle_mst , &spi_trans_lst.base );
    if ( result_lsi32 == ESP_OK ) {
      last_result_lui8 = OP_OK;
    } else {
      last_result_lui8 = OP_NOK;
    }
  }
#if (DEBUG_MODE == 1)
  printf ( "SPI WRITE: \n" );
  printf ( " - command: 0x%02x\n" , cmd_fui8 );
  printf ( " - address: 0x%02x\n" , addr_fui8 );
  printf ( " - length : 0x%02x\n" , buffer_len_fu32 );
  printf ( " - data   :" );
  for ( int i = 0; i < buffer_len_fu32; i++ ) {
    printf ( " %02X" , send_buffer_fpui8 [ i ] );
  }
  printf ( "\n" );
#endif
  return last_result_lui8;
}

StdReturn_gui8_t get_time_stamp_mui8 ( TimeStamp_gst_t * time_stamp_fpst )
{
  if (time_stamp_fpst != NULL_PTR) {
    time_stamp_fpst->time_stamp_hi_u32 = 0xFFFFFFFF;
    time_stamp_fpst->time_stamp_lo_u32 = 0xFFFFFFFF;
  }
  return OP_OK;
}

void spi_init_mvd ( spi_device_handle_t * spi_handle_fpst )
{
  esp_err_t ret_lsi32 = ESP_FAIL;
  spi_bus_config_t bus_cfg_lst;
  spi_device_interface_config_t dev_cfg_lst;

  if ( spi_handle_fpst != NULL_PTR ) {
    memset ( &bus_cfg_lst , 0 , sizeof(spi_bus_config_t) );
    memset ( &dev_cfg_lst , 0 , sizeof(spi_device_interface_config_t) );

    // configure SPI bus
    bus_cfg_lst.miso_io_num = SPI_PIN_MISO;
    bus_cfg_lst.mosi_io_num = SPI_PIN_MOSI;
    bus_cfg_lst.sclk_io_num = SPI_PIN_CLK;
    bus_cfg_lst.quadwp_io_num = -1;
    bus_cfg_lst.quadhd_io_num = -1;
    bus_cfg_lst.max_transfer_sz = SPI_BUFFER_MAX_SIZE + 2;

    // configure SPI device
    dev_cfg_lst.clock_speed_hz = SPI_CLOCK_SPEED_HZ, //Clock out at 8 MHz
    dev_cfg_lst.mode = SPI_MODE;                   //SPI mode 0
    dev_cfg_lst.spics_io_num = SPI_PIN_CS;         //CS pin
    dev_cfg_lst.queue_size = 7;                      //We want to be able to queue 7 transactions at a time

    //Initialize the SPI bus
    ret_lsi32 = spi_bus_initialize ( HSPI_HOST , &bus_cfg_lst , 1 );
    ESP_ERROR_CHECK( ret_lsi32 );
    //Attach the the CAN shield to H-SPI
    ret_lsi32 = spi_bus_add_device ( HSPI_HOST , &dev_cfg_lst , spi_handle_fpst );
    ESP_ERROR_CHECK( ret_lsi32 );
  }
}

void main_init_mvd( spi_device_handle_t * spi_handle_fpst )
{
  StdReturn_gui8_t result_lui8 = OP_NOK;
  DeviceCfg_gst_t mcp2515_config_lst;
  RXB0_ConfigSet_gst_t rx_buffer_0_cfg_lpst;
  RXB1_ConfigSet_gst_t rx_buffer_1_cfg_lpst;

  if ( spi_handle_fpst != NULL_PTR ) {
    // Init spi device
    spi_init_mvd( spi_handle_fpst );

    mcp2515_config_lst.cb_spi_write_bytes_gui8 = spi_write_mui8;
    mcp2515_config_lst.cb_time_stamp_gui8 = get_time_stamp_mui8;
    mcp2515_config_lst.operation_mode_enm = LOOPBACK_MODE_enm;
    mcp2515_config_lst.tx_mode_enm = TX_NORMAL_MODE_enm;
    // set RXB0
    rx_buffer_0_cfg_lpst.is_extended_bl = TRUE;
    rx_buffer_0_cfg_lpst.rxb0_accept_data_st.filters_pst[ 0 ] = 0x12345678;
    rx_buffer_0_cfg_lpst.rxb0_accept_data_st.filters_pst[ 1 ] = 0x76543210;
    rx_buffer_0_cfg_lpst.rxb0_accept_data_st.rx_mask_u32 = 0x1FFFFFFF;
    rx_buffer_0_cfg_lpst.roll_over_enable_bl = FALSE;
    rx_buffer_0_cfg_lpst.filters_masks_enable_bl = TRUE;
    // set RXB1
    rx_buffer_1_cfg_lpst.rxb1_accept_data_st.filters_pst[ 0 ] = 0xABCDABCD;
    rx_buffer_1_cfg_lpst.is_extended_bl = FALSE;
    rx_buffer_1_cfg_lpst.rxb1_accept_data_st.filters_pst[ 1 ] = 0XFFFFABCD;
    rx_buffer_1_cfg_lpst.rxb1_accept_data_st.filters_pst[ 2 ] = 0xABCDFFAB;
    rx_buffer_1_cfg_lpst.rxb1_accept_data_st.filters_pst[ 3 ] = 0X11111111;
    rx_buffer_1_cfg_lpst.rxb1_accept_data_st.rx_mask_u32 = 0xFFFFFFFF;
    rx_buffer_1_cfg_lpst.filters_masks_enable_bl = TRUE;
    // Copy the masks and filters
    mcp2515_config_lst.rx_buffer_0_cfg_pst = &rx_buffer_0_cfg_lpst;
    mcp2515_config_lst.rx_buffer_1_cfg_pst = &rx_buffer_1_cfg_lpst;
    // Init the mcp2515 device
    result_lui8 = device_init_gui8( &mcp2515_config_lst );
    if ( result_lui8 == OP_OK ) {
      printf( "INFO: device initialized correctly " );
    } else {
      printf( "ERROR: Initialization failed " );
    }
    printf( "ret_val 0x%02x !\n" , result_lui8 );
    esp_log_level_set( "spi_master" , ESP_LOG_NONE );
  }
}


void AtomicOpBegin_gvd ( void )
{
  portDISABLE_INTERRUPTS();
}

void AtomicOpEnd_gvd ( void )
{
  portENABLE_INTERRUPTS();
}

void DelayMs_vd ( )
{
  vTaskDelay ( 10 / portTICK_RATE_MS );
}


void mcp2515_task_10ms(void* p)
{
  while ( TRUE ) {
    mcp2515_main_gvd();
    vTaskDelay(10 / portTICK_PERIOD_MS);
  }
}

void disable_interrupts_gvd(void)
{
  //portDISABLE_INTERRUPTS();
}
void enable_interrupts_gvd(void)
{
  //portENABLE_INTERRUPTS();
}

void tx_task_10ms( void* p)
{
  TxCanContainer_gst_t tx_container_1;
  uint32_t tx_queue_usage_lu32 = 0;
  StdReturn_gui8_t last_result_lui8 = 0;
  static uint32_t counter = 0;

  while ( TRUE ) {
    if (( counter / 64) % 2 ==0) {
    tx_container_1.can_container_st.time_stamp_st.time_stamp_hi_u32 = 0;
    tx_container_1.can_container_st.time_stamp_st.time_stamp_lo_u32 = 0;
    tx_container_1.can_container_st.can_msg_data_st.frame_type_enm  = DATA_FRAME_enm;
    tx_container_1.can_container_st.can_msg_data_st.is_extd_id_bl   = TRUE;
    tx_container_1.can_container_st.can_msg_data_st.msg_dlc_ui8     = 2;
    tx_container_1.can_container_st.can_msg_data_st.msg_id_u32      = 0x12345678;
    tx_container_1.can_container_st.can_msg_data_st.msg_priority_ui8 = 3;
    for ( int i = 0; i < 2; i++ ) {
      tx_container_1.can_container_st.can_msg_data_st.data_pui8[ i ] = 0xA2;
    }
    last_result_lui8 = write_can_msg_gui8( &tx_container_1 , &tx_queue_usage_lu32 );
    printf("TX Queue usage: %02d\n", tx_queue_usage_lu32);

    printf("TX Queue usage: %02d\n", tx_queue_usage_lu32);
    tx_container_1.can_container_st.can_msg_data_st.frame_type_enm = DATA_FRAME_enm;
    tx_container_1.can_container_st.can_msg_data_st.is_extd_id_bl = FALSE;
    tx_container_1.can_container_st.can_msg_data_st.msg_dlc_ui8 = 7;
    tx_container_1.can_container_st.can_msg_data_st.msg_id_u32 = 0xABCDABCD;
    tx_container_1.can_container_st.can_msg_data_st.msg_priority_ui8 = 3;
    for ( int i = 0; i < 7; i++ ) {
      tx_container_1.can_container_st.can_msg_data_st.data_pui8[ i ] = 0xA7;
    }
    last_result_lui8 = write_can_msg_gui8( &tx_container_1 , &tx_queue_usage_lu32 );
    printf("TX Queue usage: %02d\n", tx_queue_usage_lu32);

    tx_container_1.can_container_st.can_msg_data_st.frame_type_enm = DATA_FRAME_enm;
    tx_container_1.can_container_st.can_msg_data_st.is_extd_id_bl = TRUE;
    tx_container_1.can_container_st.can_msg_data_st.msg_dlc_ui8 = 5;
    tx_container_1.can_container_st.can_msg_data_st.msg_id_u32 = 0x76543210;
    tx_container_1.can_container_st.can_msg_data_st.msg_priority_ui8 = 3;
    for ( int i = 0; i < 5; i++ ) {
      tx_container_1.can_container_st.can_msg_data_st.data_pui8[ i ] = 0xA5;
    }
    last_result_lui8 = write_can_msg_gui8( &tx_container_1 , &tx_queue_usage_lu32 );
    printf("TX Queue usage: %02d\n", tx_queue_usage_lu32);

    tx_container_1.can_container_st.can_msg_data_st.frame_type_enm = DATA_FRAME_enm;
        tx_container_1.can_container_st.can_msg_data_st.is_extd_id_bl = FALSE;
        tx_container_1.can_container_st.can_msg_data_st.msg_dlc_ui8 = 8;
        tx_container_1.can_container_st.can_msg_data_st.msg_id_u32 = 0xABCDFFAB;
        tx_container_1.can_container_st.can_msg_data_st.msg_priority_ui8 = 3;
        for ( int i = 0; i < 5; i++ ) {
          tx_container_1.can_container_st.can_msg_data_st.data_pui8[ i ] = 0xA8;
        }
        last_result_lui8 = write_can_msg_gui8( &tx_container_1 , &tx_queue_usage_lu32 );
        printf("TX Queue usage: %02d\n", tx_queue_usage_lu32);
    }
    counter++;

    vTaskDelay( 60 / portTICK_PERIOD_MS );
  }
}

void rx_task_20ms( void* p)
{
  RxCanContainer_gst_t rx_container_1;
  uint32_t rx_queue_usage_lu32 = 0;
  StdReturn_gui8_t last_result_lui8 = 0;

  while(TRUE){
    last_result_lui8 = read_can_msg_gui8(&rx_container_1, &rx_queue_usage_lu32);
    if ( last_result_lui8 == OP_OK ) {
      printf( "[%03d] : 0x%08x-0x%08x : 0x%08x : %s : %d : " ,
      /* Tx Queue usage   */rx_queue_usage_lu32 ,
      /* Time Stamp       */rx_container_1.time_stamp_st.time_stamp_hi_u32 ,
      /* Time Stamp       */rx_container_1.time_stamp_st.time_stamp_lo_u32 ,
      /* message ID       */rx_container_1.can_msg_data_st.msg_id_u32 ,
      /* is it extended ? */( rx_container_1.can_msg_data_st.is_extd_id_bl == TRUE ) ? "EXT\0" : "STD\0" ,
      /* Data length      */rx_container_1.can_msg_data_st.msg_dlc_ui8 );
      for ( int i = 0; i < rx_container_1.can_msg_data_st.msg_dlc_ui8; i++ ) {
        printf( " %0X" , rx_container_1.can_msg_data_st.data_pui8[ i ] );
      }
      printf( "\n" );
    }
    vTaskDelay( 20  / portTICK_PERIOD_MS );
  }
}

typedef struct _TaskProperties_gst_t {
  TaskFunction_t func_ptr_h;
  TaskHandle_t task_handle_pvd;
  char task_name_pui8[ 20 ];
  uint8_t task_prio_ui8;
} TaskProperties_gst_t;

void app_main( )
{
  TaskProperties_gst_t tasks_lst[ 3 ] = {
      {
          mcp2515_task_10ms,
          NULL_PTR,
          "MCP_MAIN\0",
          10
      },
      {
          tx_task_10ms,
          NULL_PTR,
          "TX_TASK\0",
          1
      },
      {
          rx_task_20ms,
          NULL_PTR,
          "RX_TASK\0",
          5
      }
  };

  // Call init function
  main_init_mvd( &spi_handle_mst );

  for ( int i = 0; i < 3; i++ ) {
    xTaskCreate(
        tasks_lst[i].func_ptr_h ,
        tasks_lst[i].task_name_pui8 ,
        4096 ,
        NULL_PTR ,
        tasks_lst[i].task_prio_ui8 ,
        &tasks_lst[i].task_handle_pvd );
  }

  while ( TRUE ) {

    vTaskDelay( 500 / portTICK_PERIOD_MS );
  }

  return;
}
// End of file app_main.c
